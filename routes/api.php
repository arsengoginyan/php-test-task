<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/posts', [\App\Http\Controllers\PostController::class, 'index']);
Route::delete('/posts/{post}', [\App\Http\Controllers\PostController::class, 'delete']);

Route::get('/comments', [\App\Http\Controllers\CommentController::class, 'index']);
Route::post('/comments', [\App\Http\Controllers\CommentController::class, 'store']);
Route::delete('/comments/{comment}', [\App\Http\Controllers\CommentController::class, 'delete']);
