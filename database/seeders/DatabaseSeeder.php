<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Post::factory(5)->create();

        $comments = self::generateComments();
        Comment::insert($comments);

    }

    protected function generateComments()
    {
        $randomWords = explode(',', strtolower("Cool,Strange,Funny,Laughing,Nice,Awesome,Great,Horrible,Beautiful,PHP,Vegeta,Italy,Joost"));
        $subsets = [[]];
        $comments = [];
        $posts = Post::take(5)->pluck('id')->toArray();

        foreach ($randomWords as $word) {
            foreach ($subsets as $subset) {
                $set = array_merge(array($word), $subset);

                if($set !== []) {
                    $subsets[] = $set;

                    $abbreviation = '';
                    foreach ($set as $item) {
                        $abbreviation .= substr($item, 0, 1);
                    }

                    $comments[] = ['post_id' => $posts[rand(0, 4)],
                        'content' => implode(' ', $set),
                        'abbreviation' => $abbreviation,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()];
                }
            }
        }

        return $comments;
    }
}
