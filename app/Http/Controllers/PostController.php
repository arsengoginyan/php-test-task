<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexPostsRequest;
use App\Models\Post;
use Carbon\Carbon;

class PostController extends Controller
{

    public function index(IndexPostsRequest $request)
    {
        $posts = Post::query()->orderBy($request['sort'] ?? 'id', $request['direction'] ?? 'asc');

        $count = $posts->count();

        $posts->when($request['id'], function ($query) use ($request) {
            return $query->where('id', $request['id']);
        })
        ->when($request['topic'], function ($query) use ($request) {
            return $query->where('topic', 'LIKE', "%{$request->topic}%");
        })
        ->when($request['created_at'], function ($query) use ($request) {
            return $query->where('created_at', '>=', Carbon::parse($request->created_at));
        })
        ->when($request['updated_at'], function ($query) use ($request) {
            return $query->where('updated_at', '>=', Carbon::parse($request->updated_at));
        })
        ->when($request['with'] === 'comments', function ($query) use ($request) {
            return $query->with('comments');
        })
        ->when($request['comment'], function ($query) use ($request) {
            return $query->whereHas('comments', function ($q) use ($request) {
                return $q->where('content', 'LIKE', "%{$request['comment']}%");
            });
        });

        return response()->json(['result' => $posts->paginate($request['limit'] ?? 10, ['*'], 'page', $request['page'] ?? 1)->items(), 'count' => $count]);
    }

    public function delete(Post $post)
    {
        return $post->delete();
    }

}
