<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexCommentsRequest;
use App\Http\Requests\StoreCommentRequest;
use App\Models\Comment;
use Carbon\Carbon;

class CommentController extends Controller
{
    public function index(IndexCommentsRequest $request)
    {
        $comments = Comment::query()->orderBy($request['sort'] ?? 'id', $request['direction'] ?? 'asc');

        $count = $comments->count();

        $comments->when($request['id'], function ($query) use ($request) {
           return $query->where('id', $request['id']);
        })
        ->when($request['content'], function ($query) use ($request) {
            return $query->where('content', 'LIKE', "%{$request['content']}%");
        })
        ->when($request['abbreviation'], function ($query) use ($request) {
            return $query->where('abbreviation', 'LIKE', "%{$request->abbreviation}%");
        })
        ->when($request['created_at'], function ($query) use ($request) {
            return $query->where('created_at', '>=', Carbon::parse($request->created_at));
        })
        ->when($request['updated_at'], function ($query) use ($request) {
            return $query->where('updated_at', '>=', Carbon::parse($request->updated_at));
        })
        ->when($request['with'] === 'post', function ($query) use ($request) {
            return $query->with('post');
        });

        return response()->json(['result' => $comments->paginate($request['limit'] ?? 10, ['*'], 'page', $request['page'] ?? 1)->items(), 'count' => $count]);
    }

    public function store(StoreCommentRequest $request)
    {
        try {
            Comment::create(['post_id' => $request['post_id'],
                            'content' => $request['content'],
                            'abbreviation' => self::generateAbbreviation($request['content']),
                            ]);
        } catch (\Throwable $e) {
            return $e->getMessage();
        }

        return true;
    }

    public function delete(Comment $comment)
    {
        return $comment->delete();
    }

    protected function generateAbbreviation(string $content): string
    {
        $words = explode(' ', $content);
        $abbreviation = '';
        foreach ($words as $word) {
            $abbreviation .= substr($word, 0, 1);
        }

        return $abbreviation;
    }

}
