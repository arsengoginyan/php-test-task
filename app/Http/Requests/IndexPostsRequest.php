<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class IndexPostsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'sort' => Rule::in(['id', 'topic', 'created_at', 'updated_at']),
            'direction' => Rule::in(['asc', 'desc']),
            'limit' => 'integer',
            'page' => 'integer',
            'id' => 'integer',
            'topic' => 'string',
            'created_at' => 'date',
            'updated_at' => 'date',
        ];
    }
}
