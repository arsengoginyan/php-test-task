<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class IndexCommentsRequest extends FormRequest
{
    public function rules()
    {
        return [
            'sort' => Rule::in(['id', 'content', 'abbreviation', 'created_at', 'updated_at']),
            'direction' => Rule::in(['asc', 'desc']),
            'id' => 'integer',
            'post_id' => 'integer',
            'content' => 'string',
            'abbreviation' => 'string',
            'created_at' => 'date',
            'updated_at' => 'date',
        ];
    }
}
